import { Request, Response, Next } from "restify";

const index = (req: Request, res: Response, next: Next) => {
    res.send("App is running...");
    next();
};

export default index;