import { Server } from "restify";
import index from "./index";

const router = (server: Server) => {
    server.get("/", index);
};

export default router;