const serverSettings = {
    port: 8000,
    origins: ["*"],
    methods: ["GET", "POST"]
}

export default serverSettings;