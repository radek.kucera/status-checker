import {ConnectionOptions} from "typeorm";

const options: ConnectionOptions = {
    "type": "mysql",
    "host": "localhost",
    "port": 3306,
    "username": "root",
    "password": "",
    "database": "status_checker"
};

export default options;