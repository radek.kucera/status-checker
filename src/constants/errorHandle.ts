export const errorHandle = {
    connectionOpenError: { errorId: 0, errorStatus: "Database were not connected successfully." },
    connectionCloseError: { errorId: 1, errorStatus: "The connection were not closed successfully."}
}

export const resultHandle = {
    connectionOpenResult: { resultId: 0, resultStatus: "Database were connected successfully."},
    connectionCloseResult: { resultId: 1, resultStatus: "The connection were closed successfully."}
}

