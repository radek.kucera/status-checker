export interface Checkers {
    readonly url: string;

    isUrlValid(): boolean;
}

export type CheckerErrorInterface = {
    checkerName: string,
    errorId: number,
    errorStatus: string
}

export interface Databases {
    connect(): Promise<ConnectionResponse>;

    executeQuery?(): any;

    disconnect(): Promise<ConnectionResponse>;
}

export type ConnectionResponse = ConnectionResult | ConnectionError;

export type ConnectionError = { errorId: number, errorStatus: string | object };

export type ConnectionResult = { resultId: number, resultStatus: string | object };

