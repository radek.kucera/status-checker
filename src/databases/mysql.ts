import {Databases, ConnectionResponse} from "../types";
import {Connection, ConnectionManager, ConnectionOptions} from "typeorm";

import mySqlOptions from "../configs/mysqlConnection";
import {errorHandle, resultHandle} from "../constants/errorHandle";

class MySql implements Databases {
    public readonly connection: Connection;
    private readonly options: ConnectionOptions;

    constructor() {
        this.options = mySqlOptions;
        this.connection = new ConnectionManager().create(this.options);
    }

    public async connect(): Promise<ConnectionResponse> {
        try {
            await this.connection.connect();
            return resultHandle.connectionOpenResult;
        } catch {
            return errorHandle.connectionOpenError;
        }
    }

    public async disconnect(): Promise<ConnectionResponse> {
        try {
            await this.connection.close();
            return resultHandle.connectionCloseResult;
        } catch {
            return errorHandle.connectionCloseError;
        }
    }
}

export default MySql;