import { createServer, Server as RestServer } from "restify";
import router from "../routes/router";

import CONFIG from "../configs/serverSettings";

class Server {
    private readonly port: number;

    private readonly server: RestServer;

    constructor() {
        this.server = createServer();
        this.port = CONFIG.port;
        this.middleware();
        this.listen();
        router(this.server);
    }

    private middleware(): void {

    }

    private listen(): void {
        this.server.listen(this.port, () => {
           console.log(`Status-checker is running on port ${this.port}`);
        });
    }
}

export default Server;