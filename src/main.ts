#!/usr/bin/env node
import Server from "./loaders/server";
import MySql from "./databases/mysql";

const server: Server = new Server();

const mySql = new MySql();

async function init() {
    const test = await mySql.connect();
    console.log(test);
    const dis = await mySql.disconnect();
    console.log(dis);
}

init();


export default server;